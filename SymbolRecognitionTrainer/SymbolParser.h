#pragma once
#include "MomentsRecognizer.h"

class SymbolParser : public MomentsRecognizer
{
public:
	SymbolParser();
	int GetInputSize();
	virtual cv::Mat MomentsToInput(fe::ComplexMoments& moments) override;
	virtual std::string OutputToValue(cv::Mat output) override;

	virtual bool Train(
		std::map<std::string, std::vector<fe::ComplexMoments>> moments,
		std::vector<int> layers,
		int max_iters = 100000,
		float eps = 0.1,
		float speed = 0.1) override;
};