#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"

#include "SymbolParser.h"

SymbolParser::SymbolParser() 
{ 
	// pAnn = cv::ml::ANN_MLP::create();
}

int SymbolParser::GetInputSize()
{
	return pAnn->getLayerSizes().at<int>(0);
}

cv::Mat SymbolParser::MomentsToInput(fe::ComplexMoments& moments)
{
	cv::Mat inpm = cv::Mat::zeros(moments.abs.size[0] * moments.abs.size[1], 1, CV_32F);
	for (int y = 0; y < moments.abs.size[1]; y++)
	{
		for (int x = 0; x < moments.abs.size[0]; x++)
		{
			inpm.at<float>(y * moments.abs.size[0] + x) = (float)(moments.abs.at<double>(y, x));
		}
	}
	return inpm;
}

std::string SymbolParser::OutputToValue(cv::Mat output)
{
	int xmax = 0;
	double max = output.at<float>(0);
	for (int x = 0; x < output.size[0]; x++)
		if (max < output.at<float>(x))
		{
			max = output.at<float>(x);
			xmax = x;
		}
	return std::to_string(xmax);
}

bool SymbolParser::Train(
	std::map<std::string, std::vector<fe::ComplexMoments>> moments,
	std::vector<int> layers,
	int max_iters, float eps, float speed)
{
	pAnn = cv::ml::ANN_MLP::create();

	if (layers.front() != (moments["0"].front().abs.size[0] * moments["0"].front().abs.size[0]))
		{ throw new std::exception("Input size mismatch!"); return false; }
	pAnn->setLayerSizes(layers);

	cv::TermCriteria termCrit(CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, max_iters, eps);
	pAnn->setTermCriteria(termCrit);
	pAnn->setActivationFunction(cv::ml::ANN_MLP::ActivationFunctions::SIGMOID_SYM, 1.0, 1.0);
	pAnn->setTrainMethod(cv::ml::ANN_MLP::TrainingMethods::BACKPROP);
	pAnn->setBackpropMomentumScale(speed);
	pAnn->setBackpropWeightScale(0.1);

	int moments_size = 0;
	for (int n = 0; n < 9; n++) { moments_size += moments[std::to_string(n)].size(); }
	cv::Mat input = cv::Mat::zeros(layers.front(), moments_size, CV_32F);
	cv::Mat response = cv::Mat::zeros(layers.back(), moments_size, CV_32F);
	moments_size = 0;
	for (int n = 0; n < 9; n++)
	{
		std::vector<fe::ComplexMoments> numom = moments[std::to_string(n)];
		for (uint i = 0; i < numom.size(); i++)
		{
			cv::Mat datarow = MomentsToInput(numom[i]);
			for (int x = 0; x < layers.front(); x++)
				{ input.at<float>(x, moments_size + i) = datarow.at<float>(x); }

			for (int x = 0; x < layers.back(); x++)
				{ response.at<float>(x, moments_size + i) = -1.0f; }
			response.at<float>(n, moments_size + i) = 1.0f;
		}
		moments_size += numom.size();
	}
	auto trainData = cv::ml::TrainData::create(input, cv::ml::COL_SAMPLE, response);
	bool done = pAnn->train(trainData, cv::ml::ANN_MLP::NO_INPUT_SCALE + cv::ml::ANN_MLP::NO_OUTPUT_SCALE);
	return pAnn->isTrained();
}