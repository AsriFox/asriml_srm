#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <time.h>

#include "Visualisation.h"
#include "FeatureExtraction.h"
#include "MomentsHelper.h"
#include "SymbolParser.h"

using namespace cv;
using namespace std;
using namespace fe;

const string dataPath = "..\\SymbolRecognitionTrainer\\Data\\";

void generateData()
{
	cout << "> How much data (%) is for training? ";
	double percent = 80.0; cin >> percent;
	bool done = MomentsHelper::DistributeData(dataPath + "labeled_data", dataPath + "ground_data", dataPath + "test_data", percent);
	if (!done) { cout << "WARNING: Data distribution haven't been completed." << endl; }

	cout << "> Polynomials power: "; int basn = 12; cin >> basn;
	cout << "> Normal size: "; int size = 128; cin >> size;
	shared_ptr<IBlobProcessor> blop = CreateBlobProcessor();
	shared_ptr<PolynomialManager> plom = CreatePolynomialManager();
	plom->InitBasis(basn, size);

	{
		map<string, vector<ComplexMoments>> resarr;
		if (MomentsHelper::GenerateMoments(dataPath + "ground_data", blop, plom, resarr))
			if (MomentsHelper::SaveMoments(dataPath + "ground_data.txt", resarr))
				cout << "> Training data has been generated successfully." << endl;
			else cout << "ERROR: Failed to save training moments to file." << endl;
		else cout << "ERROR: Failed to generate training moments." << endl;
	}
	{
		map<string, vector<ComplexMoments>> resarr;
		if (MomentsHelper::GenerateMoments(dataPath + "test_data", blop, plom, resarr))
			if (MomentsHelper::SaveMoments(dataPath + "test_data.txt", resarr))
				cout << "> Testing data has been generated successfully." << endl;
			else cout << "ERROR: Failed to save testing moments to file." << endl;
		else cout << "ERROR: Failed to generate testing moments." << endl;
	}
}

void trainNetwork()
{
	map<string, vector<ComplexMoments>> resarr;
	bool done = MomentsHelper::ReadMoments(dataPath + "ground_data.txt", resarr);
	if (done)
	{
		SymbolParser sp;
		int lmax = 2; cout << "> Number of hidden layers: "; cin >> lmax;
		int lwid = 50; cout << "> Neuron count on hidden layers: "; cin >> lwid;
		std::vector<int> config;
		config.push_back(resarr["0"].front().abs.size[0] * resarr["0"].front().abs.size[1]);
		for (uint i = 0; i < lmax; i++) { config.push_back(lwid); }
		config.push_back(9);
		bool done = sp.Train(resarr, config, 10000, 0.01f, 0.1f);
		sp.Save(dataPath + "symrec.txt");
	}
}

void precisionTest()
{
	SymbolParser sp;
	if (sp.Read(dataPath + "symrec.txt"))
	{
		cout << "> Precision test started!" << endl;
		map<string, vector<ComplexMoments>> resarr;
		if (MomentsHelper::ReadMoments(dataPath + "test_data.txt", resarr)) try {
			cout << "> Success rate: " << sp.PrecisionTest(resarr) * 100.0 << "%" << endl;
		}	catch (cv::Exception e) { cout << e.what() << endl; }
		else cout << "ERROR: Can't read test moments!" << endl;
	}
	else cout << "ERROR: Can't load the neural network!";
}

void recognizeImage()
{
	SymbolParser sp;
	if (sp.Read(dataPath + "symrec.txt"))
	{
		cout << "> Image to recognize (w/ext): " << endl;
		string filename; cin >> filename;

		shared_ptr<IBlobProcessor> blop = CreateBlobProcessor();
		shared_ptr<PolynomialManager> plom = CreatePolynomialManager();
		int basn = (int)sqrt((double)(sp.GetInputSize()));
		cout << "> Normal size: "; int size = 128; cin >> size;

		plom->InitBasis(basn, size);
		Mat image = imread(dataPath + filename, IMREAD_GRAYSCALE);
		if (image.empty()) { cout << "ERROR: Empty image" << endl; }
		else {
			imshow("Image to recognize", image);
			threshold(image, image, 127, 255, CV_THRESH_BINARY);
			SymbolParser sp;
			if (sp.Read(dataPath + "symrec.txt")) try {
				vector<Mat> blobs = blop->DetectBlobs(image);
				vector<Mat> nblobs = blop->NormalizeBlobs(blobs, size);
				vector<ComplexMoments> res;
				for (int i = 0; i < nblobs.size(); i++)
				{
					res.push_back(plom->Decompose(nblobs[i]));
					string num = sp.Recognize(res.back());
					cout << "> Recognized number: " << num << endl;
					ShowBlobDecomposition(to_string(i) + ") Recognized number: " + num, nblobs[i], plom->Recovery(res.back()));
				}
				waitKey();
				destroyAllWindows();
			}
			catch (cv::Exception e) { cout << e.what() << endl; }
			else { cout << "ERROR: Unable to read neural network from file!" << endl; }
		}
	}
}

int main(int argc, char** argv)
{
	string key;
	do 
	{
		cout << "===Enter next values to do something:===" << endl;
		cout << "  '1' - to generate data." << endl;
		cout << "  '2' - to train network." << endl;
		cout << "  '3' - to check recognizing precision." << endl;
		cout << "  '4' - to recognize single image." << endl;
		cout << "  'exit' - to close the application." << endl;
		cin >> key;
		cout << endl;
		if (key == "1") {
			generateData();
		}
		else if (key == "2") {
			trainNetwork();
		}
		else if (key == "3") {
			precisionTest();
		}
		else if (key == "4") {
			recognizeImage();
		}
		cout << endl;
	} while (key != "exit");
	return 0;
}