#define FEATURE_DLL_EXPORTS
#include "FeatureExtraction.h"
#include "RadialFunctions.h"

namespace fe
{
	class ChebyshevManager : public PolynomialManager
	{
	public:
		virtual ComplexMoments Decompose(cv::Mat blob) override;
		virtual cv::Mat Recovery(ComplexMoments & decomposition) override;
		virtual void InitBasis(int n_max, int diameter) override;
		virtual std::string GetType() override;
	};

	class CBlobProcessor : public IBlobProcessor
	{
	public:
		CBlobProcessor() {};
		virtual std::vector<cv::Mat> DetectBlobs(cv::Mat image) override;
		virtual std::vector<cv::Mat> NormalizeBlobs(std::vector<cv::Mat> & blobs, int side) override;
		virtual std::string GetType() override;
	};
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ������� ������ ChebyshevManager - ���������� PolynomialManager:

fe::ComplexMoments fe::ChebyshevManager::Decompose(cv::Mat blob)
{
	fe::ComplexMoments Result;
	Result.re = cv::Mat::zeros(polynomials.size(), polynomials[0].size(), CV_64FC1);
	Result.im = cv::Mat::zeros(polynomials.size(), polynomials[0].size(), CV_64FC1);
	Result.abs = cv::Mat::zeros(polynomials.size(), polynomials[0].size(), CV_64FC1);
	Result.phase = cv::Mat::zeros(polynomials.size(), polynomials[0].size(), CV_64FC1);
	double norma = polynomials.size() * polynomials[0].size();
	cv::Mat blum; blob.convertTo(blum, CV_64FC1);
	for (uint n = 0; n < polynomials.size(); n++)
		for (uint m = 0; m < polynomials[0].size(); m++)
		{
			double temp = blum.dot(polynomials[n][m].first);
			double tabs = polynomials[n][m].first.dot(polynomials[n][m].first);
			Result.re.at<double>(n, m) = (abs(temp) > 1e-20 ? (temp / tabs) / norma : 0.0);

			temp = blum.dot(polynomials[n][m].second);
			tabs = polynomials[n][m].second.dot(polynomials[n][m].second);
			Result.im.at<double>(n, m) = (abs(temp) > 1e-20 ? (temp / tabs) / norma : 0.0);

			Result.abs.at<double>(n, m) = sqrt(Result.re.at<double>(n, m) * Result.re.at<double>(n, m) + Result.im.at<double>(n, m) * Result.im.at<double>(n, m));
			Result.phase.at<double>(n, m) = atan2(Result.im.at<double>(n, m), Result.re.at<double>(n, m));
		}
	return Result;
}

cv::Mat fe::ChebyshevManager::Recovery(ComplexMoments & decomposition)
{
	cv::Mat Result = cv::Mat::zeros(polynomials[0][0].first.size[0], polynomials[0][0].first.size[1], CV_64FC1);
	for (uint n = 0; n < polynomials.size(); n++)
		for (uint m = 0; m < polynomials[0].size(); m++)
		{
			Result += polynomials[n][m].first * decomposition.re.at<double>(n, m);
			Result += polynomials[n][m].second * decomposition.im.at<double>(n, m);
		}
	return Result;
}

void fe::ChebyshevManager::InitBasis(int n_max, int diameter)
{
	polynomials.resize(n_max);
	for (uint n = 0; n < n_max; n++)
	{
		polynomials[n].resize(n_max);
		for (uint m = 0; m < n_max; m++)
		{
			polynomials[n][m].first = cv::Mat::zeros(diameter, diameter, CV_64FC1);
			polynomials[n][m].second = cv::Mat::zeros(diameter, diameter, CV_64FC1);
			for (int x = 0; x < diameter; x++)
			{
				for (int y = 0; y < diameter; y++)
				{
					double r = sqrt((x - diameter / 2) * (x - diameter / 2) + (y - diameter / 2) * (y - diameter / 2)) * 2.0 / (double)diameter;
					if (r > 1.0)
					{
						polynomials[n][m].first.at<double>(x, y) = 0.0;
						polynomials[n][m].second.at<double>(x, y) = 0.0;
					}
					else
					{
						if (r < 0.01)
						{
							r = r;
						}
						double pr = rf::RadialFunctions::ShiftedChebyshev(r, n);
						double pa = m * atan2(y - diameter / 2, x - diameter / 2);
						polynomials[n][m].first.at<double>(x, y) = pr * cos(pa);
						polynomials[n][m].second.at<double>(x, y) = pr * sin(pa);
					}
				}
			}
		}
	}
}

std::string fe::ChebyshevManager::GetType()
{
	return "Chebyshev polynomes manager by Oleg Vladimirovich Nikitin";
}

fe::OrthoBasis fe::PolynomialManager::GetBasis()
{
	return polynomials;
}

fe::PolynomialManager::~PolynomialManager() = default;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ������� ������ CBlobProcessor - ���������� ���������� IBlobProcessor:

std::vector<cv::Mat> fe::CBlobProcessor::DetectBlobs(cv::Mat image)
{
	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Vec4i> hierarchy;
	cv::findContours(
		image, contours, hierarchy,
		cv::RetrievalModes::RETR_TREE,
		cv::ContourApproximationModes::CHAIN_APPROX_SIMPLE
		);

	std::vector<cv::Mat> blums;
	for (int idx = 1; idx >= 0; idx = hierarchy[idx][0])
	{
		cv::Point2f center;	float radius;
		minEnclosingCircle(contours[idx], center, radius);
		radius = (int)radius + 1;
		center.x = radius - (int)center.x;
		center.y = radius - (int)center.y;
		blums.push_back(cv::Mat::zeros(2 * radius, 2 * radius, CV_8UC1));
		cv::drawContours(
			blums.back(), contours, idx,
			cv::Scalar(255, 255, 255), cv::FILLED, cv::LINE_8,
			hierarchy, 4, center);
	}
	return blums;
}

std::vector<cv::Mat> fe::CBlobProcessor::NormalizeBlobs(std::vector<cv::Mat> & blobs, int side)
{
	std::vector<cv::Mat> nblums = blobs;
	for (int i = 0; i < nblums.size(); i++)
	{
		cv::resize(blobs[i], nblums[i], cv::Size(side, side));
		//cv::GaussianBlur(nblums[i], nblums[i], cv::Size(0, 0), 4.0, 4.0);
	}
	return nblums;
}

std::string fe::CBlobProcessor::GetType()
{
	return "Blob detector by Oleg Vladimirovich Nikitin";
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ������� ���������� FeatureExtraction - ���������� �������:

std::shared_ptr<fe::IBlobProcessor> fe::CreateBlobProcessor()
{
	return std::make_shared<fe::CBlobProcessor>();
}

std::shared_ptr<fe::PolynomialManager> fe::CreatePolynomialManager()
{
	return std::make_shared<fe::ChebyshevManager>();
}

std::string fe::GetTestString()
{
	return "You successfuly plug feature extraction library!";
}

