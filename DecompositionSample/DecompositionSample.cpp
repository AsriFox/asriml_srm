#include <iostream>
#include "Visualisation.h"
#include <FeatureExtraction.h>

using namespace std;
using namespace cv;

int main() 
{
	cout << fe::GetTestString().c_str() << endl;
	shared_ptr<fe::PolynomialManager> pPolyMan = fe::CreatePolynomialManager();
	cout << pPolyMan->GetType() << endl;

	// ������������ ������:
	int nmax = 12; int dia = 64;
	pPolyMan->InitBasis(nmax, dia);
	fe::OrthoBasis OBS = pPolyMan->GetBasis();
	ShowPolynomials("Polynomial orthogonal basis", OBS);
	waitKey();

	// �������� �����������:
	Mat numage = imread("..\\Debug\\numbers_2.png", CV_LOAD_IMAGE_GRAYSCALE);	//CV_8UC1
	Mat newmage;
	threshold(numage, newmage, 127.0, 255.0, THRESH_BINARY);
	cv::imshow("New image", newmage);
	cv::waitKey();

	// ����������� ��������:
	shared_ptr<fe::IBlobProcessor> pIBP = fe::CreateBlobProcessor();
	cout << pIBP->GetType() << endl;
	vector<Mat> blums = pIBP->DetectBlobs(newmage);	
	vector<Mat> nblums = pIBP->NormalizeBlobs(blums, dia);

	for (int i = 0; i < nblums.size(); i++)
	{
		// ���������� � �������������� �������:
		fe::ComplexMoments cm = pPolyMan->Decompose(nblums[i]);
		Mat blum = pPolyMan->Recovery(cm);
		ShowBlobDecomposition("Number " + to_string(i), nblums[i], blum);
	}
	waitKey();

	return 0;
}