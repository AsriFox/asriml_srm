#include <iostream>
#include <time.h>
#include <AsriML_Nerv.h>

int main()
{
	printf((ANN::GetTestString() + "\n").c_str());

	std::vector<std::vector<float>> inputs, outputs;
	/*
	for (int i = 0; i < 4; i++)
	{
	std::vector<float> buf;
	buf.push_back(i / 2); buf.push_back(i % 2);
	inputs.push_back(buf);
	buf.clear();	
	buf.push_back((i / 2) ^ (i % 2));
	outputs.push_back(buf);
	}

	if (SaveData("xor.data", inputs, outputs))
	cout << "Data has been successfully written to file.";
	else
	cout << "An error occured while saving data.";
	*/
	if (ANN::LoadData("..\\Debug\\xor_train.data", inputs, outputs))
	{
		std::vector<size_t> config;
		config.push_back(2); config.push_back(4); config.push_back(1);
		std::shared_ptr<ANN::ANeuralNetwork> pANN = ANN::CreateNeuralNetwork(config);
		srand(time(0));
		pANN->RandomInit();
		ANN::BackPropTraining(pANN, inputs, outputs, 20000, 0.001f, 1.0f, true);
		printf((pANN->GetType() + "\n").c_str());
		if (pANN->Save("..\\Debug\\xor.ann"))
			printf("Neural network saved.\n");
	}
	else printf("Unable to read the training data!\n");

	system("pause");
	return 0;
}